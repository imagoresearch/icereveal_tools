#include "savexmldialog.h"
#include "ui_savexmldialog.h"
#include <QFileDialog>
#include <QMessageBox>

saveXMLDialog::saveXMLDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::saveXMLDialog)
{
    ui->setupUi(this);
    fileName = "myxml";
    ui->saveAsTextEdit->setText(fileName);
}

saveXMLDialog::~saveXMLDialog()
{
    delete ui;
}

void saveXMLDialog::on_browseButton_clicked()
{
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Open Directory"));
    ui->whereTextEdit->setText(fileName);
}

void saveXMLDialog::on_saveXMLButton_clicked()
{
    fileName = ui->saveAsTextEdit->toPlainText();
    location = ui->whereTextEdit->toPlainText();
    if (fileName == "" || location == ""){
        QMessageBox msgBox;
        msgBox.setText("Please specify the file name and location!");
        msgBox.exec();
    }
    else{
        close();
    }
}

void saveXMLDialog::on_cancelButton_clicked()
{
    close();
}
