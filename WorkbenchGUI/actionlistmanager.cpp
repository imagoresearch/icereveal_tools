#include "actionlistmanager.h"
#include <QXmlStreamWriter>
#include <QtXml>
#include <QFile>
#include "action.h"
#include <QDebug>

ActionListManager::ActionListManager()
{
    currentIndex = 0;

    globIndex = 0;

    // Initialize filter library
    filterParam.initializeFilterLibrary();
}

int ActionListManager::writeXML(QString xmlFile)
{
    QFile file(xmlFile);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)){
        return -1;
    }

    QXmlStreamWriter xmlWriter(&file);
    // Set the auto-formatting text
    xmlWriter.setAutoFormatting(true);
    xmlWriter.writeStartDocument();
    xmlWriter.writeStartElement("ICE_Algorithm");
    int numActionList = actionListsFile.count();
    for (int i = 0; i < numActionList; i++)
    {
        xmlWriter.writeStartElement(actionListsFile[i].listName);
        // add actions for each action list
        int numActions = actionListsFile[i].list.count();
        for (int j = 0; j < numActions; j++)
        {
            // add filter name
            Action filter = actionListsFile[i].list[j];
            xmlWriter.writeStartElement(filter.name);

            // add parameters for the filter
            QMapIterator<QString, QString> i(filter.parameters);
            while (i.hasNext()) {
                i.next();
                QString key = i.key();
                QString value = i.value();
                xmlWriter.writeStartElement(key);
                xmlWriter.writeCharacters(value);
                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();
        }
        xmlWriter.writeEndElement();
    }
    xmlWriter.writeEndElement();
    xmlWriter.writeEndDocument();
    file.close();

    return 0;
}

int ActionListManager::loadXML(QString xmlFile)
{
    QFile file(xmlFile);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)){
        return -1;
    }

    QDomDocument xmlBOM;
    // Set data into the QDomDocument before processing
    xmlBOM.setContent(&file);

    QVector<ActionList> listFile;

    QDomElement root = xmlBOM.documentElement();
    if (root.tagName() != "ICE_Algorithm") return -2;
    QDomElement actionListNode = root.firstChildElement();
    while (!actionListNode.isNull()){
        ActionList actionList;
        actionList.listName = actionListNode.tagName();
        QDomElement actionNode = actionListNode.firstChildElement();
        while (!actionNode.isNull()){
            Action action;
            action.name = actionNode.tagName();
            QDomElement parameterNode = actionNode.firstChildElement();
            while (!parameterNode.isNull()){
                QString name = parameterNode.tagName();
                QString value = parameterNode.text();
                action.parameters.insert(name, value);
                qDebug() << name;
                qDebug() << value;
                parameterNode = parameterNode.nextSiblingElement();
            }
            actionList.list.append(action);
            actionNode = actionNode.nextSiblingElement();
        }
        listFile.append(actionList);
        actionListNode = actionListNode.nextSiblingElement();
    }

    if (listFile.count() > 0){
        actionListsFile.clear();
        actionListsFile.append(listFile);
    }

    file.close();


    return 0;
}
