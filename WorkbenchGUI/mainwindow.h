#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "actionlistmanager.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

    ActionListManager actionListMnager;

private slots:

    void on_createActionListButton_clicked();

    void on_saveXMLButton_clicked();

    void on_deleteButton_clicked();

    void on_inputDirectoryBrowse_clicked();

    void on_outputDirectoryBrowse_clicked();

    void on_xmlFileBrowse_clicked();

    void on_runButton_clicked();

    void on_loadXMLButton_clicked();

    void on_actionLists_doubleClicked(const QModelIndex &index);

    void on_ampFileBrowseButton_clicked();

private:
    Ui::MainWindow *ui;

    int nActionList;
};

#endif // MAINWINDOW_H
