#ifndef ACTION_H
#define ACTION_H

#include <QMap>

class Action
{
public:
    Action();

    QMap<QString, QString> parameters;
    QString name;
};

#endif // ACTION_H
