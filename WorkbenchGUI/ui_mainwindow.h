/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QTabWidget *tabWidget;
    QWidget *tab;
    QLabel *label_2;
    QListWidget *actionLists;
    QPushButton *deleteButton;
    QPushButton *createActionListButton;
    QPushButton *loadXMLButton;
    QPushButton *saveXMLButton;
    QWidget *tab_2;
    QTextEdit *inputDirectoryTextEdit;
    QTextEdit *outputDirectoryTextEdit;
    QTextEdit *xmlFileTextEdit;
    QPushButton *inputDirectoryBrowse;
    QPushButton *outputDirectoryBrowse;
    QPushButton *xmlFileBrowse;
    QLabel *label;
    QLabel *label_3;
    QLabel *label_4;
    QPushButton *runButton;
    QTextEdit *ampFileTextEdit;
    QPushButton *ampFileBrowseButton;
    QLabel *label_5;
    QMenuBar *menuBar;
    QToolBar *mainToolBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(645, 495);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        tabWidget = new QTabWidget(centralWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        tabWidget->setGeometry(QRect(40, 40, 581, 381));
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        label_2 = new QLabel(tab);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(30, 30, 111, 31));
        QFont font;
        font.setPointSize(18);
        label_2->setFont(font);
        actionLists = new QListWidget(tab);
        actionLists->setObjectName(QStringLiteral("actionLists"));
        actionLists->setGeometry(QRect(20, 70, 271, 151));
        deleteButton = new QPushButton(tab);
        deleteButton->setObjectName(QStringLiteral("deleteButton"));
        deleteButton->setGeometry(QRect(20, 220, 81, 41));
        createActionListButton = new QPushButton(tab);
        createActionListButton->setObjectName(QStringLiteral("createActionListButton"));
        createActionListButton->setGeometry(QRect(290, 70, 121, 41));
        loadXMLButton = new QPushButton(tab);
        loadXMLButton->setObjectName(QStringLiteral("loadXMLButton"));
        loadXMLButton->setGeometry(QRect(290, 120, 121, 41));
        saveXMLButton = new QPushButton(tab);
        saveXMLButton->setObjectName(QStringLiteral("saveXMLButton"));
        saveXMLButton->setGeometry(QRect(420, 290, 131, 41));
        tabWidget->addTab(tab, QString());
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        inputDirectoryTextEdit = new QTextEdit(tab_2);
        inputDirectoryTextEdit->setObjectName(QStringLiteral("inputDirectoryTextEdit"));
        inputDirectoryTextEdit->setGeometry(QRect(160, 40, 251, 41));
        outputDirectoryTextEdit = new QTextEdit(tab_2);
        outputDirectoryTextEdit->setObjectName(QStringLiteral("outputDirectoryTextEdit"));
        outputDirectoryTextEdit->setGeometry(QRect(160, 110, 251, 41));
        xmlFileTextEdit = new QTextEdit(tab_2);
        xmlFileTextEdit->setObjectName(QStringLiteral("xmlFileTextEdit"));
        xmlFileTextEdit->setGeometry(QRect(160, 240, 251, 41));
        inputDirectoryBrowse = new QPushButton(tab_2);
        inputDirectoryBrowse->setObjectName(QStringLiteral("inputDirectoryBrowse"));
        inputDirectoryBrowse->setGeometry(QRect(420, 40, 91, 41));
        outputDirectoryBrowse = new QPushButton(tab_2);
        outputDirectoryBrowse->setObjectName(QStringLiteral("outputDirectoryBrowse"));
        outputDirectoryBrowse->setGeometry(QRect(420, 110, 91, 41));
        xmlFileBrowse = new QPushButton(tab_2);
        xmlFileBrowse->setObjectName(QStringLiteral("xmlFileBrowse"));
        xmlFileBrowse->setGeometry(QRect(420, 240, 91, 41));
        label = new QLabel(tab_2);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(40, 50, 91, 21));
        label_3 = new QLabel(tab_2);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(40, 110, 111, 21));
        label_4 = new QLabel(tab_2);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(40, 250, 91, 21));
        runButton = new QPushButton(tab_2);
        runButton->setObjectName(QStringLiteral("runButton"));
        runButton->setGeometry(QRect(220, 310, 114, 41));
        runButton->setFont(font);
        ampFileTextEdit = new QTextEdit(tab_2);
        ampFileTextEdit->setObjectName(QStringLiteral("ampFileTextEdit"));
        ampFileTextEdit->setGeometry(QRect(160, 180, 251, 41));
        ampFileBrowseButton = new QPushButton(tab_2);
        ampFileBrowseButton->setObjectName(QStringLiteral("ampFileBrowseButton"));
        ampFileBrowseButton->setGeometry(QRect(420, 180, 91, 41));
        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(40, 190, 91, 21));
        tabWidget->addTab(tab_2, QString());
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 645, 22));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);

        retranslateUi(MainWindow);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0));
        label_2->setText(QApplication::translate("MainWindow", "Action List", 0));
        deleteButton->setText(QApplication::translate("MainWindow", "Delete", 0));
        createActionListButton->setText(QApplication::translate("MainWindow", "New Action list", 0));
        loadXMLButton->setText(QApplication::translate("MainWindow", "Load from XML", 0));
        saveXMLButton->setText(QApplication::translate("MainWindow", "Save To XML>>", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("MainWindow", "Create Action List", 0));
        inputDirectoryBrowse->setText(QApplication::translate("MainWindow", "Browse", 0));
        outputDirectoryBrowse->setText(QApplication::translate("MainWindow", "Browse", 0));
        xmlFileBrowse->setText(QApplication::translate("MainWindow", "Browse", 0));
        label->setText(QApplication::translate("MainWindow", "Input Directory", 0));
        label_3->setText(QApplication::translate("MainWindow", "Output Directory", 0));
        label_4->setText(QApplication::translate("MainWindow", "XML file", 0));
        runButton->setText(QApplication::translate("MainWindow", "Run", 0));
        ampFileBrowseButton->setText(QApplication::translate("MainWindow", "Browse", 0));
        label_5->setText(QApplication::translate("MainWindow", "AMP folder", 0));
        tabWidget->setTabText(tabWidget->indexOf(tab_2), QApplication::translate("MainWindow", "Run Action list", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
