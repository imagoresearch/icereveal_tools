#ifndef ACTIONLIST_H
#define ACTIONLIST_H

#include <QString>
#include <QVector>
#include "action.h"

class ActionList
{
public:
    ActionList();

    QVector<Action> list;
    QString listName;
};

#endif // ACTIONLIST_H
