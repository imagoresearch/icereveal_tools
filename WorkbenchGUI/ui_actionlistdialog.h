/********************************************************************************
** Form generated from reading UI file 'actionlistdialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_ACTIONLISTDIALOG_H
#define UI_ACTIONLISTDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QPushButton>

QT_BEGIN_NAMESPACE

class Ui_actionListDialog
{
public:
    QGroupBox *groupBox;
    QListWidget *filtersLibraryList;
    QLabel *label;
    QPushButton *addToActionlistButton;
    QPushButton *saveListButton;
    QPushButton *deleteButton;
    QPushButton *cancelButton;

    void setupUi(QDialog *actionListDialog)
    {
        if (actionListDialog->objectName().isEmpty())
            actionListDialog->setObjectName(QStringLiteral("actionListDialog"));
        actionListDialog->resize(727, 613);
        QFont font;
        font.setPointSize(15);
        actionListDialog->setFont(font);
        groupBox = new QGroupBox(actionListDialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(350, 40, 331, 451));
        filtersLibraryList = new QListWidget(actionListDialog);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        new QListWidgetItem(filtersLibraryList);
        filtersLibraryList->setObjectName(QStringLiteral("filtersLibraryList"));
        filtersLibraryList->setGeometry(QRect(30, 61, 221, 311));
        label = new QLabel(actionListDialog);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(90, 20, 131, 31));
        QFont font1;
        font1.setPointSize(18);
        label->setFont(font1);
        addToActionlistButton = new QPushButton(actionListDialog);
        addToActionlistButton->setObjectName(QStringLiteral("addToActionlistButton"));
        addToActionlistButton->setGeometry(QRect(260, 180, 91, 41));
        saveListButton = new QPushButton(actionListDialog);
        saveListButton->setObjectName(QStringLiteral("saveListButton"));
        saveListButton->setGeometry(QRect(580, 560, 101, 41));
        deleteButton = new QPushButton(actionListDialog);
        deleteButton->setObjectName(QStringLiteral("deleteButton"));
        deleteButton->setGeometry(QRect(350, 490, 81, 31));
        cancelButton = new QPushButton(actionListDialog);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));
        cancelButton->setGeometry(QRect(470, 560, 101, 41));

        retranslateUi(actionListDialog);

        QMetaObject::connectSlotsByName(actionListDialog);
    } // setupUi

    void retranslateUi(QDialog *actionListDialog)
    {
        actionListDialog->setWindowTitle(QApplication::translate("actionListDialog", "Dialog", 0));
        groupBox->setTitle(QApplication::translate("actionListDialog", "Config Action list", 0));

        const bool __sortingEnabled = filtersLibraryList->isSortingEnabled();
        filtersLibraryList->setSortingEnabled(false);
        QListWidgetItem *___qlistwidgetitem = filtersLibraryList->item(0);
        ___qlistwidgetitem->setText(QApplication::translate("actionListDialog", "median_filter", 0));
        QListWidgetItem *___qlistwidgetitem1 = filtersLibraryList->item(1);
        ___qlistwidgetitem1->setText(QApplication::translate("actionListDialog", "conv_filter", 0));
        QListWidgetItem *___qlistwidgetitem2 = filtersLibraryList->item(2);
        ___qlistwidgetitem2->setText(QApplication::translate("actionListDialog", "invert_filter", 0));
        QListWidgetItem *___qlistwidgetitem3 = filtersLibraryList->item(3);
        ___qlistwidgetitem3->setText(QApplication::translate("actionListDialog", "levels_filter", 0));
        QListWidgetItem *___qlistwidgetitem4 = filtersLibraryList->item(4);
        ___qlistwidgetitem4->setText(QApplication::translate("actionListDialog", "histomap_filter", 0));
        QListWidgetItem *___qlistwidgetitem5 = filtersLibraryList->item(5);
        ___qlistwidgetitem5->setText(QApplication::translate("actionListDialog", "replace_color_filterV1", 0));
        QListWidgetItem *___qlistwidgetitem6 = filtersLibraryList->item(6);
        ___qlistwidgetitem6->setText(QApplication::translate("actionListDialog", "replace_color_filterV2", 0));
        QListWidgetItem *___qlistwidgetitem7 = filtersLibraryList->item(7);
        ___qlistwidgetitem7->setText(QApplication::translate("actionListDialog", "hue_saturation_lightness_filter", 0));
        QListWidgetItem *___qlistwidgetitem8 = filtersLibraryList->item(8);
        ___qlistwidgetitem8->setText(QApplication::translate("actionListDialog", "doSobel_Weighted_Gray_3x3", 0));
        QListWidgetItem *___qlistwidgetitem9 = filtersLibraryList->item(9);
        ___qlistwidgetitem9->setText(QApplication::translate("actionListDialog", "doColor_To_Grayscale", 0));
        QListWidgetItem *___qlistwidgetitem10 = filtersLibraryList->item(10);
        ___qlistwidgetitem10->setText(QApplication::translate("actionListDialog", "exposure_filter", 0));
        QListWidgetItem *___qlistwidgetitem11 = filtersLibraryList->item(11);
        ___qlistwidgetitem11->setText(QApplication::translate("actionListDialog", "binarize_filter", 0));
        QListWidgetItem *___qlistwidgetitem12 = filtersLibraryList->item(12);
        ___qlistwidgetitem12->setText(QApplication::translate("actionListDialog", "unsharp_mask_filter", 0));
        QListWidgetItem *___qlistwidgetitem13 = filtersLibraryList->item(13);
        ___qlistwidgetitem13->setText(QApplication::translate("actionListDialog", "multiply", 0));
        filtersLibraryList->setSortingEnabled(__sortingEnabled);

        label->setText(QApplication::translate("actionListDialog", "Filters Library", 0));
        addToActionlistButton->setText(QApplication::translate("actionListDialog", ">>", 0));
        saveListButton->setText(QApplication::translate("actionListDialog", "Save list", 0));
        deleteButton->setText(QApplication::translate("actionListDialog", "Delete", 0));
        cancelButton->setText(QApplication::translate("actionListDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class actionListDialog: public Ui_actionListDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_ACTIONLISTDIALOG_H
