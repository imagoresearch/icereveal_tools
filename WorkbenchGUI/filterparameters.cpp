#include "filterparameters.h"

FilterParameters::FilterParameters()
{

}

void FilterParameters::initializeParameters()
{
    medianFilter.insert("radius", "1");

    convFilter.insert("kernel", "0 0 0 -1 3 0 0 -1 0");

    levelsFilter.insert("low", "0");
    levelsFilter.insert("gamma", "0.1");
    levelsFilter.insert("high", "255");

    histomapFilter.insert("ampFile", "Color_Island_1.amp");

    hueSaturationLightnessFilter.insert("hue", "0");
    hueSaturationLightnessFilter.insert("sat", "0.2");
    hueSaturationLightnessFilter.insert("light", "0");
    hueSaturationLightnessFilter.insert("color", "Green");

    replaceColorFilterV1.insert("low", "0 0 0");
    replaceColorFilterV1.insert("high", "255 255 255");
    replaceColorFilterV1.insert("to", "255 255 255");

    replaceColorFilterV2.insert("from", "0 0 0");
    replaceColorFilterV2.insert("to", "255 255 255");
    replaceColorFilterV2.insert("fuzziness", "1.0");

    exposureFilter.insert("exposure", "1.0");

    binarizeFilter.insert("threshold", "0");

    unsharpMaskFilter.insert("amount", "10");
    unsharpMaskFilter.insert("radius", "1");
    unsharpMaskFilter.insert("threshold", "10");

    multiply.insert("factor", "0.7");

    sobelFilter.insert("bUSE_SUMS_OF_IMAGE_GRADIENTSf", "1");
    sobelFilter.insert("bVERSION_2_OF_SOBEL_3x3f", "1");
    sobelFilter.insert("bUSING_ONLY_BLACK_AND_WHITE_COLORSf", "0");
    sobelFilter.insert("bBLACK_BACKGROUND_WHITE_CONTOURSf", "1");
    sobelFilter.insert("nIntensityThresholdForSobelMinf", "1");
    sobelFilter.insert("nIntensityThresholdForSobelMaxf", "75");
    sobelFilter.insert("fFadingFactorf", "0.8");
    sobelFilter.insert("fWeight_For_2nd_Versionf", "2.3");
    sobelFilter.insert("fNormalizingf", "1.0");
    sobelFilter.insert("fWeight_Redf", "80");
    sobelFilter.insert("fWeight_Greenf", "10");
    sobelFilter.insert("fWeight_Bluef", "10");

    doColorToGrayscale.insert("bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf", "0");
    doColorToGrayscale.insert("fFadingFactorf", "1");
    doColorToGrayscale.insert("fWeight_Redf", "100");
    doColorToGrayscale.insert("fWeight_Greenf", "100");
    doColorToGrayscale.insert("fWeight_Bluef", "0");
    doColorToGrayscale.insert("fWeight_Cyanf", "0");
    doColorToGrayscale.insert("fWeight_Magentaf", "0");
    doColorToGrayscale.insert("fWeight_Yellowf", "200");
    doColorToGrayscale.insert("fWeight_Lf", "0");
    doColorToGrayscale.insert("fWeight_Af", "0");
    doColorToGrayscale.insert("fWeight_Bf", "0");
    doColorToGrayscale.insert("fWeight_Convert_RedToGrayscalef", "60");
    doColorToGrayscale.insert("fWeight_Convert_GreenToGrayscalef", "40");
    doColorToGrayscale.insert("fWeight_Convert_BlueToGrayscalef", "0");
    doColorToGrayscale.insert("bINCLUDE_EXPf", "0");
    doColorToGrayscale.insert("nThreshPixelIntensityForExpf", "20");
    doColorToGrayscale.insert("fConstForExpf", "0.005");
    doColorToGrayscale.insert("bINCLUDE_PIXEL_INTENSITY_SUBSTITUTIONf", "0");
    doColorToGrayscale.insert("nPixelIntensToBeSubstituted_Minf", "50");
    doColorToGrayscale.insert("nPixelIntensToBeSubstituted_Maxf", "140");
    doColorToGrayscale.insert("nPixelIntensToSubstitutef", "50");
    doColorToGrayscale.insert("bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSINGf", "0");
    doColorToGrayscale.insert("bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf", "0");
    doColorToGrayscale.insert("fWeightOfHuef", "50");
    doColorToGrayscale.insert("fWeightOfLightnessf", "140");
    doColorToGrayscale.insert("fWeightOfSaturationf", "50");
    doColorToGrayscale.insert("nHueMinf", "300");
    doColorToGrayscale.insert("nHueMaxf", "360");
    doColorToGrayscale.insert("bSELECTING_COLOR_RANGES_AT_THE_READINGf", "0");
    doColorToGrayscale.insert("bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUTf", "0");
    doColorToGrayscale.insert("nRedMinf", "140");
    doColorToGrayscale.insert("nRedMaxf", "180");
    doColorToGrayscale.insert("nGreenMinf", "10");
    doColorToGrayscale.insert("nGreenMaxf", "60");
    doColorToGrayscale.insert("nBlueMinf", "30");
    doColorToGrayscale.insert("nBlueMaxf", "80");

}

void FilterParameters::initializeFilterLibrary()
{
    QMap<QString, QString> medianFilter;
    QMap<QString, QString> convFilter;
    QMap<QString, QString> levelsFilter;
    QMap<QString, QString> invertFilter;
    QMap<QString, QString> hueSaturationLightnessFilter;
    QMap<QString, QString> histomapFilter;
    QMap<QString, QString> replaceColorFilterV1;
    QMap<QString, QString> replaceColorFilterV2;
    QMap<QString, QString> unsharpMaskFilter;
    QMap<QString, QString> binarizeFilter;
    QMap<QString, QString> exposureFilter;
    QMap<QString, QString> sobelFilter;
    QMap<QString, QString> doColorToGrayscale;
    QMap<QString, QString> multiply;

    medianFilter.insert("radius", "1");
    filterLibrary.insert("median_filter", medianFilter);

    convFilter.insert("kernel", "0 0 0 -1 3 0 0 -1 0");
    filterLibrary.insert("conv_filter", convFilter);

    levelsFilter.insert("low", "0");
    levelsFilter.insert("gamma", "0.1");
    levelsFilter.insert("high", "255");
    filterLibrary.insert("levels_filter", levelsFilter);

    histomapFilter.insert("ampFile", "Color_Island_1.amp");
    filterLibrary.insert("histomap_filter", histomapFilter);

    hueSaturationLightnessFilter.insert("hue", "0");
    hueSaturationLightnessFilter.insert("sat", "0.2");
    hueSaturationLightnessFilter.insert("light", "0");
    hueSaturationLightnessFilter.insert("color", "Green");
    filterLibrary.insert("hue_saturation_lightness_filter", hueSaturationLightnessFilter);

    replaceColorFilterV1.insert("low", "0 0 0");
    replaceColorFilterV1.insert("high", "255 255 255");
    replaceColorFilterV1.insert("to", "255 255 255");
    filterLibrary.insert("replace_color_filterV1", replaceColorFilterV1);

    replaceColorFilterV2.insert("from", "0 0 0");
    replaceColorFilterV2.insert("to", "255 255 255");
    replaceColorFilterV2.insert("fuzziness", "1.0");
    filterLibrary.insert("replace_color_filterV2", replaceColorFilterV2);

    exposureFilter.insert("exposure", "1.0");
    filterLibrary.insert("exposure_filter", exposureFilter);

    binarizeFilter.insert("threshold", "0");
    filterLibrary.insert("binarize_filter", binarizeFilter);

    unsharpMaskFilter.insert("amount", "10");
    unsharpMaskFilter.insert("radius", "1");
    unsharpMaskFilter.insert("threshold", "10");
    filterLibrary.insert("unsharp_mask_filter", unsharpMaskFilter);

    multiply.insert("factor", "0.7");
    filterLibrary.insert("multiply", multiply);

    sobelFilter.insert("bUSE_SUMS_OF_IMAGE_GRADIENTSf", "1");
    sobelFilter.insert("bVERSION_2_OF_SOBEL_3x3f", "1");
    sobelFilter.insert("bUSING_ONLY_BLACK_AND_WHITE_COLORSf", "0");
    sobelFilter.insert("bBLACK_BACKGROUND_WHITE_CONTOURSf", "1");
    sobelFilter.insert("nIntensityThresholdForSobelMinf", "1");
    sobelFilter.insert("nIntensityThresholdForSobelMaxf", "75");
    sobelFilter.insert("fFadingFactorf", "0.8");
    sobelFilter.insert("fWeight_For_2nd_Versionf", "2.3");
    sobelFilter.insert("fNormalizingf", "1.0");
    sobelFilter.insert("fWeight_Redf", "80");
    sobelFilter.insert("fWeight_Greenf", "10");
    sobelFilter.insert("fWeight_Bluef", "10");
    filterLibrary.insert("doSobel_Weighted_Gray_3x3", sobelFilter);

    doColorToGrayscale.insert("bIS_HISTOGRAM_EQUALIZATION_INCLUDEDf", "0");
    doColorToGrayscale.insert("fFadingFactorf", "1");
    doColorToGrayscale.insert("fWeight_Redf", "100");
    doColorToGrayscale.insert("fWeight_Greenf", "100");
    doColorToGrayscale.insert("fWeight_Bluef", "0");
    doColorToGrayscale.insert("fWeight_Cyanf", "0");
    doColorToGrayscale.insert("fWeight_Magentaf", "0");
    doColorToGrayscale.insert("fWeight_Yellowf", "200");
    doColorToGrayscale.insert("fWeight_Lf", "0");
    doColorToGrayscale.insert("fWeight_Af", "0");
    doColorToGrayscale.insert("fWeight_Bf", "0");
    doColorToGrayscale.insert("fWeight_Convert_RedToGrayscalef", "60");
    doColorToGrayscale.insert("fWeight_Convert_GreenToGrayscalef", "40");
    doColorToGrayscale.insert("fWeight_Convert_BlueToGrayscalef", "0");
    doColorToGrayscale.insert("bINCLUDE_EXPf", "0");
    doColorToGrayscale.insert("nThreshPixelIntensityForExpf", "20");
    doColorToGrayscale.insert("fConstForExpf", "0.005");
    doColorToGrayscale.insert("bINCLUDE_PIXEL_INTENSITY_SUBSTITUTIONf", "0");
    doColorToGrayscale.insert("nPixelIntensToBeSubstituted_Minf", "50");
    doColorToGrayscale.insert("nPixelIntensToBeSubstituted_Maxf", "140");
    doColorToGrayscale.insert("nPixelIntensToSubstitutef", "50");
    doColorToGrayscale.insert("bINCLUDE_RGB_FROM_WEIGHTED_HLS_BEFORE_PROCESSINGf", "0");
    doColorToGrayscale.insert("bINCLUDE_RGB_FROM_WEIGHTED_HLS_AFTER_PROCESSINGf", "0");
    doColorToGrayscale.insert("fWeightOfHuef", "50");
    doColorToGrayscale.insert("fWeightOfLightnessf", "140");
    doColorToGrayscale.insert("fWeightOfSaturationf", "50");
    doColorToGrayscale.insert("nHueMinf", "300");
    doColorToGrayscale.insert("nHueMaxf", "360");
    doColorToGrayscale.insert("bSELECTING_COLOR_RANGES_AT_THE_READINGf", "0");
    doColorToGrayscale.insert("bSELECTING_COLOR_RANGES_AT_THE_HLS_OUTPUTf", "0");
    doColorToGrayscale.insert("nRedMinf", "140");
    doColorToGrayscale.insert("nRedMaxf", "180");
    doColorToGrayscale.insert("nGreenMinf", "10");
    doColorToGrayscale.insert("nGreenMaxf", "60");
    doColorToGrayscale.insert("nBlueMinf", "30");
    doColorToGrayscale.insert("nBlueMaxf", "80");
    filterLibrary.insert("doColor_To_Grayscale", doColorToGrayscale);

}
