#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDebug>
#include "actionlistdialog.h"
#include "savexmldialog.h"
#include <QProcess>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    // keep track of the index of the action list created sofar and set up list name accordingly
    nActionList = 0;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_createActionListButton_clicked()
{
    actionListMnager.currentIndex = -1;
    int size = actionListMnager.actionListsFile.count();

    actionListDialog dlg(nullptr, &actionListMnager);
    dlg.setModal(true);
    dlg.exec();

    int num = actionListMnager.actionListsFile.count() - size;
    if (num > 0)
    {
        QString actionListName = actionListMnager.actionListsFile[actionListMnager.actionListsFile.count()-1].listName;
        ui->actionLists->addItem(actionListName);
        actionListMnager.globIndex++;
        nActionList++;
    }
}

void MainWindow::on_saveXMLButton_clicked()
{
    if (ui->actionLists->count() == 0) return;

    saveXMLDialog dlg;
    dlg.setModal(true);
    dlg.exec();
    QString xmlFileName = dlg.fileName;
    QString xmlFilePath = dlg.location;
    if (!xmlFileName.isEmpty() && !xmlFilePath.isEmpty()){
        QString xmlFile = xmlFilePath + "/" + xmlFileName + ".xml";
        actionListMnager.writeXML(xmlFile);
    }
}

void MainWindow::on_deleteButton_clicked()
{
    // delete selected action list
    int selectedIndex = ui->actionLists->currentIndex().row();
    if (selectedIndex >= 0)
    {
        delete ui->actionLists->takeItem(selectedIndex);
        actionListMnager.actionListsFile.remove(selectedIndex);
    }
}

void MainWindow::on_inputDirectoryBrowse_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Open Directory"));
    ui->inputDirectoryTextEdit->setText(directory);
}

void MainWindow::on_outputDirectoryBrowse_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Open Directory"));
    ui->outputDirectoryTextEdit->setText(directory);
}

void MainWindow::on_xmlFileBrowse_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open XML"), "",  tr("XML Files (*.xml)"));
    ui->xmlFileTextEdit->setText(fileName);
}

void MainWindow::on_runButton_clicked()
{
    // TODO: run exe here
    QString program = "reveal";
    QStringList arguments;
    QString inputDirectory = ui->inputDirectoryTextEdit->toPlainText();
    QString outputDirectory = ui->outputDirectoryTextEdit->toPlainText();
    QString ampfile = ui->ampFileTextEdit->toPlainText();
    QString xmlFile = ui->xmlFileTextEdit->toPlainText();
    if (inputDirectory.isEmpty() || outputDirectory.isEmpty() || ampfile.isEmpty() || xmlFile.isEmpty()){
        return;
    }
    arguments << inputDirectory << outputDirectory << ampfile << xmlFile;

    QProcess *myProcess = new QProcess();
    myProcess->start(program, arguments);
}

void MainWindow::on_loadXMLButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open XML"), "",  tr("XML Files (*.xml)"));

    // load xml file and display in gui
    int status = actionListMnager.loadXML(fileName);
    if (status < 0) return;

    // update action list widget
    for (int i = 0; i < ui->actionLists->count(); i++){
        delete ui->actionLists->takeItem(i);
    }
    int numActionList = actionListMnager.actionListsFile.count();
    for (int i = 0; i < numActionList; i++){
        QString listName = actionListMnager.actionListsFile[i].listName;
        ui->actionLists->addItem(listName);
    }
}

void MainWindow::on_actionLists_doubleClicked(const QModelIndex &index)
{
    int selectedIndex = index.row(); //ui->actionLists->currentIndex().row();
    if (selectedIndex >= 0)
    {
        actionListMnager.currentIndex = selectedIndex;
        actionListDialog dlg(nullptr, &actionListMnager);
        dlg.setModal(true);
        dlg.exec();
    }

    // update action list name
    QString name = actionListMnager.actionListsFile[actionListMnager.currentIndex].listName;
    ui->actionLists->item(selectedIndex)->setText(name);
}

void MainWindow::on_ampFileBrowseButton_clicked()
{
    QString directory = QFileDialog::getExistingDirectory(this, tr("Open Directory"));
    ui->ampFileTextEdit->setText(directory);
}
