#ifndef ACTIONLISTMANAGER_H
#define ACTIONLISTMANAGER_H

#include <QVector>
#include "actionlist.h"
#include "filterparameters.h"

class ActionListManager
{
public:
    ActionListManager();

    int writeXML(QString xmlFile);
    int loadXML(QString xmlFile);

    QVector<ActionList> actionListsFile;
    int currentIndex;
    int globIndex;

    FilterParameters filterParam;
};

#endif // ACTIONLISTMANAGER_H
