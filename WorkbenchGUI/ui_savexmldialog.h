/********************************************************************************
** Form generated from reading UI file 'savexmldialog.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SAVEXMLDIALOG_H
#define UI_SAVEXMLDIALOG_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QTextEdit>

QT_BEGIN_NAMESPACE

class Ui_saveXMLDialog
{
public:
    QGroupBox *groupBox;
    QPushButton *saveXMLButton;
    QLabel *label_6;
    QPushButton *browseButton;
    QTextEdit *whereTextEdit;
    QTextEdit *saveAsTextEdit;
    QLabel *label_5;
    QPushButton *cancelButton;

    void setupUi(QDialog *saveXMLDialog)
    {
        if (saveXMLDialog->objectName().isEmpty())
            saveXMLDialog->setObjectName(QStringLiteral("saveXMLDialog"));
        saveXMLDialog->resize(484, 278);
        groupBox = new QGroupBox(saveXMLDialog);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(20, 30, 431, 211));
        QFont font;
        font.setPointSize(18);
        groupBox->setFont(font);
        saveXMLButton = new QPushButton(groupBox);
        saveXMLButton->setObjectName(QStringLiteral("saveXMLButton"));
        saveXMLButton->setGeometry(QRect(323, 160, 91, 41));
        QFont font1;
        font1.setPointSize(14);
        saveXMLButton->setFont(font1);
        label_6 = new QLabel(groupBox);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 100, 71, 31));
        label_6->setFont(font1);
        browseButton = new QPushButton(groupBox);
        browseButton->setObjectName(QStringLiteral("browseButton"));
        browseButton->setGeometry(QRect(330, 100, 81, 41));
        browseButton->setFont(font1);
        whereTextEdit = new QTextEdit(groupBox);
        whereTextEdit->setObjectName(QStringLiteral("whereTextEdit"));
        whereTextEdit->setGeometry(QRect(80, 100, 251, 41));
        saveAsTextEdit = new QTextEdit(groupBox);
        saveAsTextEdit->setObjectName(QStringLiteral("saveAsTextEdit"));
        saveAsTextEdit->setGeometry(QRect(80, 50, 251, 41));
        label_5 = new QLabel(groupBox);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 50, 71, 31));
        label_5->setFont(font1);
        cancelButton = new QPushButton(groupBox);
        cancelButton->setObjectName(QStringLiteral("cancelButton"));
        cancelButton->setGeometry(QRect(240, 160, 81, 41));
        cancelButton->setFont(font1);

        retranslateUi(saveXMLDialog);

        QMetaObject::connectSlotsByName(saveXMLDialog);
    } // setupUi

    void retranslateUi(QDialog *saveXMLDialog)
    {
        saveXMLDialog->setWindowTitle(QApplication::translate("saveXMLDialog", "Dialog", 0));
        groupBox->setTitle(QApplication::translate("saveXMLDialog", "Save to XML", 0));
        saveXMLButton->setText(QApplication::translate("saveXMLDialog", "Save", 0));
        label_6->setText(QApplication::translate("saveXMLDialog", "Where", 0));
        browseButton->setText(QApplication::translate("saveXMLDialog", "Browse", 0));
        label_5->setText(QApplication::translate("saveXMLDialog", "Save As", 0));
        cancelButton->setText(QApplication::translate("saveXMLDialog", "Cancel", 0));
    } // retranslateUi

};

namespace Ui {
    class saveXMLDialog: public Ui_saveXMLDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SAVEXMLDIALOG_H
