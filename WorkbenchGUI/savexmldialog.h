#ifndef SAVEXMLDIALOG_H
#define SAVEXMLDIALOG_H

#include <QDialog>

namespace Ui {
class saveXMLDialog;
}

class saveXMLDialog : public QDialog
{
    Q_OBJECT

public:
    explicit saveXMLDialog(QWidget *parent = nullptr);
    ~saveXMLDialog();

    QString fileName;
    QString location;
private slots:
    void on_browseButton_clicked();

    void on_saveXMLButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::saveXMLDialog *ui;
};

#endif // SAVEXMLDIALOG_H
