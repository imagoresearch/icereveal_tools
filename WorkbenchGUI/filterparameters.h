#ifndef FILTERPARAMETERS_H
#define FILTERPARAMETERS_H
#include <QMap>

class FilterParameters
{
public:
    FilterParameters();
    void initializeParameters();
    void initializeFilterLibrary();

    QMap<QString, QString> medianFilter;
    QMap<QString, QString> convFilter;
    QMap<QString, QString> levelsFilter;
    QMap<QString, QString> invertFilter;
    QMap<QString, QString> hueSaturationLightnessFilter;
    QMap<QString, QString> histomapFilter;
    QMap<QString, QString> replaceColorFilterV1;
    QMap<QString, QString> replaceColorFilterV2;
    QMap<QString, QString> unsharpMaskFilter;
    QMap<QString, QString> binarizeFilter;
    QMap<QString, QString> exposureFilter;
    QMap<QString, QString> sobelFilter;
    QMap<QString, QString> doColorToGrayscale;
    QMap<QString, QString> multiply;

    QMap<QString, QMap<QString, QString> > filterLibrary;
};

#endif // FILTERPARAMETERS_H
