#include "actionlistdialog.h"
#include "ui_actionlistdialog.h"
#include <QListWidget>
#include "customtreewidget.h"
#include <QVBoxLayout>
#include <QFileDialog>
#include "action.h"
#include "actionlist.h"

actionListDialog::actionListDialog(QWidget *parent, ActionListManager *manager) :
    QDialog(parent),
    ui(new Ui::actionListDialog)
{
    ui->setupUi(this);

    pManager = manager;

    widget = new CustomTreeWidget();
    widget->setDragDropMode(QAbstractItemView::InternalMove);
    widget->setColumnCount(2);
    widget->setColumnWidth(0,200);
    QVBoxLayout* viewLayout = new QVBoxLayout;
    viewLayout->addWidget(widget);
    ui->groupBox->setLayout(viewLayout);

    params.initializeParameters();

    QString name = "ActionList" + QString::number(pManager->globIndex);
    if (pManager && pManager->currentIndex >= 0 && pManager->actionListsFile.count() > 0){
        name = pManager->actionListsFile[pManager->currentIndex].listName;
    }
    addRoot(name);
    if (pManager && pManager->currentIndex >= 0 && pManager->actionListsFile.count() > 0)
    {
        QTreeWidgetItem *parentItem = widget->topLevelItem(0);
        parentItem->setExpanded(true);
        ActionList actionList = pManager->actionListsFile[pManager->currentIndex];
        for (int i = 0; i < actionList.list.count(); i++)
        {
            Action action = actionList.list[i];
            QString name = action.name;
            QTreeWidgetItem *filter = addChild(parentItem, name, "");
            // add parameters node for each action
            QMapIterator<QString, QString> it(action.parameters);
            while (it.hasNext()) {
                it.next();
                QString key = it.key();
                QString value = it.value();
                QTreeWidgetItem *it = addChild(filter, key, value);
                it->setFlags(Qt::ItemIsEditable|Qt::ItemIsEnabled);
            }
        }
    }
}

actionListDialog::~actionListDialog()
{
    delete ui;
}

void actionListDialog::actionListDialog::addRoot(QString name)
{
    QTreeWidgetItem *parentItem;
    parentItem = new QTreeWidgetItem(widget);
    parentItem->setText(0, name);
    parentItem->setFlags(Qt::ItemIsEditable|Qt::ItemIsEnabled);
}

QTreeWidgetItem* actionListDialog::addChild(QTreeWidgetItem *parent, QString name, QString value)
{
    QTreeWidgetItem *item = new QTreeWidgetItem(parent);
    item->setText(0, name);
    item->setText(1, value);
    parent->addChild(item);
    return item;
}

void actionListDialog::createNode(QTreeWidgetItem *child, QMap<QString, QString> &map)
{
    QMapIterator<QString, QString> i(map);
    while (i.hasNext()) {
        i.next();
        QString name, value;
        QTreeWidgetItem *it = addChild(child, i.key(), i.value());
        it->setFlags(Qt::ItemIsEditable|Qt::ItemIsEnabled);
    }
    // add input and saveOutput parameter
    QTreeWidgetItem *it = addChild(child, "input", "");
    it->setFlags(Qt::ItemIsEditable|Qt::ItemIsEnabled);
    it = addChild(child, "saveOutput", "0");
    it->setFlags(Qt::ItemIsEditable|Qt::ItemIsEnabled);
}

void actionListDialog::on_addToActionlistButton_clicked()
{
    QListWidgetItem *item = ui->filtersLibraryList->currentItem();
    if (item){
        QString filter = item->text();
        QTreeWidgetItem *child, *parentItem;
        parentItem = widget->topLevelItem(0);
        parentItem->setExpanded(true);
        child = addChild(parentItem, filter, "");
        if (filter == "median_filter"){
            createNode(child, params.medianFilter);
        }else if (filter == "conv_filter"){
            createNode(child, params.convFilter);
        }else if (filter == "levels_filter"){
            createNode(child, params.levelsFilter);
        }else if (filter == "histomap_filter"){
            createNode(child, params.histomapFilter);
        }else if (filter == "hue_saturation_lightness_filter"){
            createNode(child, params.hueSaturationLightnessFilter);
        }else if (filter == "replace_color_filterV1"){
            createNode(child, params.replaceColorFilterV1);
        }else if (filter == "replace_color_filterV2"){
            createNode(child, params.replaceColorFilterV2);
        }else if (filter == "unsharp_mask_filter"){
            createNode(child, params.unsharpMaskFilter);
        }else if (filter == "binarize_filter"){
            createNode(child, params.binarizeFilter);
        }else if (filter == "exposure_filter"){
            createNode(child, params.exposureFilter);
        }else if (filter == "doSobel_Weighted_Gray_3x3"){
            createNode(child, params.sobelFilter);
        }else if (filter == "doColor_To_Grayscale"){
            createNode(child, params.doColorToGrayscale);
        }else if (filter == "invert_filter"){
            createNode(child, params.invertFilter);
        }else if (filter == "multiply"){
            createNode(child, params.multiply);
        }
    }
}

void actionListDialog::on_deleteButton_clicked()
{
    QTreeWidgetItem *selectedItem = widget->currentItem();
    QTreeWidgetItem *parentItem = selectedItem->parent();
    if (parentItem){
        // delete the selected action
        delete parentItem->takeChild(parentItem->indexOfChild(selectedItem));
    }else{
        // the root node is selected, delete all the actions
        int numChild = selectedItem->childCount();
        for (int i = 0; i < numChild; i++){
            delete selectedItem->takeChild(0);
        }
    }
}

void actionListDialog::on_saveListButton_clicked()
{
    // save to action list
    ActionList actionList;
    QTreeWidgetItem *filter, *param, *parentItem;
    parentItem = widget->topLevelItem(0);
    int numAction = parentItem->childCount();
    for (int i = 0; i < numAction; i++){
        filter = parentItem->child(i);
        Action action;
        action.name = filter->text(0);
        int numParameters = filter->childCount();
        for (int j = 0; j < numParameters; j++){
            param = filter->child(j);
            action.parameters.insert(param->text(0), param->text(1));
        }
        actionList.list.append(action);
    }

    actionList.listName = parentItem->text(0);
    if (numAction > 0){
        if (pManager->currentIndex >= 0){
            pManager->actionListsFile[pManager->currentIndex] = actionList;
        }else{
            pManager->actionListsFile.append(actionList);
        }
    }
    close();
}

void actionListDialog::on_cancelButton_clicked()
{
    close();
}
