#ifndef ACTIONLISTDIALOG_H
#define ACTIONLISTDIALOG_H

#include <QDialog>
//#include <QtCore>
//#include <QtGui>
#include <QTreeWidgetItem>
#include "filterparameters.h"
#include "actionlistmanager.h"

namespace Ui {
class actionListDialog;
}

class actionListDialog : public QDialog
{
    Q_OBJECT

    void addRoot(QString name);
    QTreeWidgetItem * addChild(QTreeWidgetItem * parent, QString name, QString value);
    void createNode(QTreeWidgetItem *child, QMap<QString, QString> &map);
public:
    explicit actionListDialog(QWidget *parent = nullptr, ActionListManager *manager = nullptr);
    ~actionListDialog();

    FilterParameters params;

    ActionListManager *pManager;

private slots:
    void on_addToActionlistButton_clicked();

    void on_deleteButton_clicked();

    void on_saveListButton_clicked();

    void on_cancelButton_clicked();

private:
    Ui::actionListDialog *ui;
    QTreeWidget *widget;
};

#endif // ACTIONLISTDIALOG_H
