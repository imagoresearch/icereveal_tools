#include "customtreewidget.h"


CustomTreeWidget::CustomTreeWidget(QWidget* parent):QTreeWidget(parent)
{
}

void CustomTreeWidget::dragEnterEvent(QDragEnterEvent *event){
    draggedItem = currentItem();
    dragIndex = indexAt(event->pos());
    QTreeWidget::dragEnterEvent(event);
}

void CustomTreeWidget::dropEvent(QDropEvent *event){
    QModelIndex droppedIndex = indexAt(event->pos());
    if( !droppedIndex.isValid() )
        return;

    if(draggedItem){

        QTreeWidgetItem* dParent = draggedItem->parent();
        if(dParent){
            if(itemFromIndex(droppedIndex.parent()) != dParent)
                return;
            dParent->removeChild(draggedItem);
            dParent->insertChild(droppedIndex.row(), draggedItem);
        }
    }
}
