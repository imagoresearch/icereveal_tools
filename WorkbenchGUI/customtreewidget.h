#ifndef CUSTOMTREEWIDGET_H
#define CUSTOMTREEWIDGET_H
#include <QTreeWidget>
#include <QDragEnterEvent>

class CustomTreeWidget : public QTreeWidget
{
public:
    CustomTreeWidget(QWidget *parent = nullptr);
protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);
private:
    QTreeWidgetItem* draggedItem;
    QModelIndex dragIndex;
};

#endif // CUSTOMTREEWIDGET_H
