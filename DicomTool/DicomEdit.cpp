//
//  DcmToJpeg.cpp
//  ICEReveal
//
//  Created by Anna Yang on 9/19/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#include <stdio.h>
#include <sstream>
#include <fstream>

#include "dcmtk/dcmdata/dcfilefo.h"
#include "dcmtk/dcmdata/dctagkey.h"
#include "dcmtk/dcmdata/dcdeftag.h"

#include "dcmtk/config/osconfig.h"    /* make sure OS specific configuration is included first */
#include "dcmtk/dcmdata/dctk.h"
#include "dcmtk/dcmdata/dcpxitem.h"

#include "dcmtk/dcmjpls/djencode.h"
#include "dcmtk/dcmjpls/djdecode.h"
#include "dcmtk/dcmjpls/djrparam.h"
#include "dcmtk/dcmjpls/libcharls/intrface.h"
#include "dcmtk/dcmimage/diregist.h"

#include "fileUtil.h"

struct viewInfo{
    string fileName;
    string studyDate;      
    string studyTime;
    string studyInstanceUID;
};
struct modifiedMetadata{
    string newPatientName;
    string newPatientID;
    string accessionNumber;
    string studyDate;
};
OFString manufacturerModel, manufacturer;
vector<string> tomoView, otherView;
vector<vector<string> > ctSlices(4);
int followUp[4];
vector<vector<viewInfo> > standard2dView(4);  //LCC, LMLO, RCC, RMLO

// Date: YYYYMMDD  Time:HHMMSS
time_t convertDateTime(string date, string time)
{
    string dateTime = date + time;
    int year = 0, month = 0, day = 0, hour = 0, min = 0, sec = 0;
    time_t result;
    sscanf(dateTime.c_str(), "%4d%2d%2d%2d%2d%2d", &year, &month, &day, &hour, &min, &sec);
    struct tm breakdown = {0};
    breakdown.tm_year = year - 1900; /* years since 1900 */
    breakdown.tm_mon = month - 1;
    breakdown.tm_mday = day;
    breakdown.tm_hour = hour;
    breakdown.tm_min = min;
    breakdown.tm_sec = sec;
    result = mktime(&breakdown);
    return result;
}

bool isTomo(string fileName)
{
    vector<size_t> pos;
    size_t ps = 0, prevPos = 0;
    string prevFile = fileName;
    while(true)
    {
        string curFile = prevFile.substr(ps);
        ps = curFile.find_first_of("\\");
        if (ps == string::npos) break;
        pos.push_back(ps+prevPos);
        prevPos = ps+prevPos + 1;
        ps = ps + 1;
        prevFile = curFile;
    }
    for (int i = 0; i < pos.size()-1; i++)
    {
        string str = fileName.substr(pos[i]+1, pos[i+1]-pos[i]-1);
        if (str == "TOMO" || str == "VOLUME")
        {
            return true;
        }
    }
    return false;
}

int collectViews(string dcmFile)
{
    DcmFileFormat fileformat;
    OFCondition status = fileformat.loadFile(dcmFile.c_str());
    if (!status.good()){
        cout << "Fail to read the image: " << dcmFile << endl;
        return -1;
    }
    OFString sopClass;
    fileformat.getDataset()->findAndGetOFString(DCM_SOPClassUID, sopClass);
    if (sopClass == "1.2.840.10008.5.1.4.1.1.2"){
        OFString laterality, viewPosition, viewCode;
        fileformat.getDataset()->findAndGetOFString(DCM_ImageLaterality, laterality);
        DcmItem *item;
        if (fileformat.getDataset()->findAndGetSequenceItem(DCM_ViewCodeSequence, item).good())
        {
            item->findAndGetOFString(DCM_CodeValue, viewCode);
            if (viewCode == "R-10242")
            {
                viewPosition = "CC";
            }
            else if (viewCode == "R-10226")
            {
                viewPosition = "MLO";
            }
        }
        vector<string> *slices = &ctSlices[0];
        int viewIndex = 0;
        if (laterality == "L" && viewPosition == "CC")
        {
            slices = &ctSlices[0];
            viewIndex = 0;
        }
        if (laterality == "L" && viewPosition == "MLO")
        {
            slices = &ctSlices[1];
            viewIndex = 1;
        }
        if (laterality == "R" && viewPosition == "CC")
        {
            slices = &ctSlices[2];
            viewIndex = 2;
        }
        if (laterality == "R" && viewPosition == "MLO")
        {
            slices = &ctSlices[3];
            viewIndex = 3;
        }
        if (slices->size() == 0){
            slices->push_back(dcmFile);
        }else{
            int k;
            for (k = 0; k < slices->size(); k++){
                if (dcmFile.compare(*(slices->begin()+k)) < 0){
                    slices->insert(slices->begin()+k, dcmFile);
                    break;
                }
            }
            if (k == slices->size()){
                slices->push_back(dcmFile);
            }
        }
        
        return 0;
    }
    if (sopClass != "1.2.840.10008.5.1.4.1.1.1.2" && sopClass != "1.2.840.10008.5.1.4.1.1.1.2.1" && sopClass != "1.2.840.10008.5.1.4.1.1.13.1.3"){
        cout << "Unsupported SOP class: " << sopClass << endl;
        return -2;
    }
    
    OFString laterality, viewPosition, viewCode, imageType, studyDate, studyTime, studyInstanceUID;
    fileformat.getDataset()->findAndGetOFString(DCM_ImageLaterality, laterality);
    fileformat.getDataset()->findAndGetOFString(DCM_ViewPosition, viewPosition);
    fileformat.getDataset()->findAndGetOFStringArray(DCM_ImageType, imageType);
    fileformat.getDataset()->findAndGetOFString(DCM_ContentDate, studyDate);
    fileformat.getDataset()->findAndGetOFString(DCM_ContentTime, studyTime);
    fileformat.getDataset()->findAndGetOFString(DCM_StudyInstanceUID, studyInstanceUID);
    fileformat.getDataset()->findAndGetOFString(DCM_ManufacturerModelName, manufacturerModel);
    fileformat.getDataset()->findAndGetOFString(DCM_Manufacturer, manufacturer);
    DcmItem *item;
    if (fileformat.getDataset()->findAndGetSequenceItem(DCM_ViewCodeSequence, item).good())
    {
        item->findAndGetOFString(DCM_CodeValue, viewCode);
        DcmItem *subItem = nullptr;
        item->findAndGetSequenceItem(DCM_ViewModifierCodeSequence, subItem);
        if (subItem != nullptr){
            subItem->findAndGetOFString(DCM_CodeValue, viewCode);
        }
    }
    
    OFString numberOfFrames;
    fileformat.getDataset()->findAndGetOFString(DCM_NumberOfFrames, numberOfFrames);
    int nFrames = numberOfFrames.empty()? 1 : stoi(string(numberOfFrames.c_str()));
    if (isTomo(string(imageType.c_str())) || nFrames > 1){
        tomoView.push_back(dcmFile);
    }
    else if (viewCode == "R-10242" || viewCode == "R-10226"){
        viewInfo info;
        info.fileName = dcmFile;
        info.studyDate = string(studyDate.c_str());
        info.studyTime = string(studyTime.c_str());
        info.studyTime = string(studyTime.c_str());
        info.studyInstanceUID = string(studyInstanceUID.c_str());
        vector<viewInfo> *view = &standard2dView[0];
        int viewIndex = 0;
        if (laterality == "L" && viewPosition == "CC")
        {
            view = &standard2dView[0];
            viewIndex = 0;
        }
        if (laterality == "L" && viewPosition == "MLO")
        {
            view = &standard2dView[1];
            viewIndex = 1;
        }
        if (laterality == "R" && viewPosition == "CC")
        {
            view = &standard2dView[2];
            viewIndex = 2;
        }
        if (laterality == "R" && viewPosition == "MLO")
        {
            view = &standard2dView[3];
            viewIndex = 3;
        }
        
        // If multiple views exsit, sort them by study date and time
        if (view->size() == 0){
            view->push_back(info);
        }
        else{
            time_t time2 = convertDateTime(info.studyDate, info.studyTime);
            int i;
            for (i = 0; i < view->size(); i++){
                time_t time1 = convertDateTime((view->begin()+i)->studyDate, view->back().studyTime);
                int diff = difftime(time1, time2);
                if (diff > 0){
                    view->insert(view->begin()+i, info);
                    break;
                }
            }
            if (i == view->size()){
                view->push_back(info);
            }
        }
        // Find the follow up data if it exsits
        followUp[viewIndex] = (int)view->size();
        if (view->size() > 1){
            for (int i = 1; i < view->size(); i++){
                if ((view->begin()+i)->studyInstanceUID != (view->begin()+i-1)->studyInstanceUID){
                    followUp[viewIndex] = i;
                }
            }
        }
    }
    else{
        otherView.push_back(dcmFile);
    }
    return 0;
}

bool modifyMetaData(string dcmFile, modifiedMetadata metaData, bool fixPriors, string filePath, string suffix)
{
    DcmFileFormat fileformat;
    OFCondition status = fileformat.loadFile(dcmFile.c_str());
    if (!status.good()){
        cout << "Fail to read the image: " << dcmFile << endl;
        return false;
    }
    
    OFString patientName, patientID;
    if (fileformat.getDataset()->findAndGetOFString(DCM_PatientName, patientName).good())
    {
        
    }else{
        cout << "The Patient name field is missing!" << endl;
        return false;
    }
    
    if (fileformat.getDataset()->findAndGetOFString(DCM_PatientID, patientID).good())
    {
        
    }else{
        cout << "The Patient ID field is missing!" << endl;
        return false;
    }
    
    OFString laterality, viewPosition, viewCode;
    fileformat.getDataset()->findAndGetOFString(DCM_ImageLaterality, laterality);
    DcmItem *item;
    if (fileformat.getDataset()->findAndGetSequenceItem(DCM_ViewCodeSequence, item).good())
    {
        item->findAndGetOFString(DCM_CodeValue, viewCode);
        DcmItem *subItem = nullptr;
        item->findAndGetSequenceItem(DCM_ViewModifierCodeSequence, subItem);
        if (subItem != nullptr){
            subItem->findAndGetOFString(DCM_CodeValue, viewCode);
        }
    }
    size_t pos = viewCode.find_first_of("-");
    viewCode = viewCode.substr(pos+1);
    fileformat.getDataset()->putAndInsertString(DCM_PatientName, metaData.newPatientName.c_str());
    fileformat.getDataset()->putAndInsertString(DCM_PatientID, metaData.newPatientID.c_str());
    
    if (fixPriors)
    {
        fileformat.getDataset()->putAndInsertString(DCM_AccessionNumber, metaData.accessionNumber.c_str());
        if (metaData.studyDate != "0")
        {
            fileformat.getDataset()->putAndInsertString(DCM_StudyDate, metaData.studyDate.c_str());
        }
    }
    
    pos = filePath.find_last_of("/");
    string caseFolder = filePath.substr(pos+1);
    string sq = "\'";
    string cmd = "mkdir -p " + sq + filePath + sq;
    system(cmd.c_str());
    string saveToFile = filePath + "/" + string(metaData.newPatientID.c_str()) + "_" + string(laterality.c_str()) + "_" + string(viewCode.c_str()) + suffix + ".dcm";
    status = fileformat.saveFile(saveToFile.c_str());
    
    return true;
}

int getCompressedPixelData(Uint8 *pEncoded, int &dataLength, DcmFileFormat &fileFormat)
{
    DcmElement* element = NULL;
    OFCondition result = fileFormat.getDataset()->findAndGetElement(DCM_PixelData, element);
    if (result.bad() || element == NULL)
        return 1;
    DcmPixelData *dpix = NULL;
    dpix = OFstatic_cast(DcmPixelData*, element);
    
    DcmPixelSequence *dseq = NULL;
    E_TransferSyntax xferSyntax = EXS_Unknown;
    const DcmRepresentationParameter *rep = NULL;
    // Find the key that is needed to access the right representation of the data within DCMTK
    dpix->getOriginalRepresentationKey(xferSyntax, rep);
    // Access original data representation and get result within pixel sequence
    result = dpix->getEncapsulatedRepresentation(xferSyntax, rep, dseq);
    // Skipping offset table
    int index = 1, cur = 0;
    Uint32 length;
    DcmPixelItem* pixitem = NULL;
    while (true)
    {
        dseq->getItem(pixitem, index++);
        if (pixitem == NULL)
            break;
        Uint8* pixData = NULL;
        // Get the length of this pixel item (i.e. fragment, i.e. most of the time, the lenght of the frame)
        length = pixitem->getLength();
        if (length == 0)
            return -1;
        // Finally, get the compressed data for this pixel item
        result = pixitem->getUint8Array(pixData);
        for (int i = 0; i < length; i++)
        {
            pEncoded[i+cur] = pixData[i];
        }
        cur += length;
    }
    dataLength = cur;
    return 0;
}

bool createTomo(vector<string> &slices, string newPatientName, string newPatientID, string filePath)
{
    int numFrames = (int)slices.size();
    DcmFileFormat fileformat;
    OFCondition status = fileformat.loadFile(slices[0].c_str());
    Uint16 width, height, samplePerPixel;
    fileformat.getDataset()->findAndGetUint16(DCM_Columns, width);
    fileformat.getDataset()->findAndGetUint16(DCM_Rows, height);
    fileformat.getDataset()->findAndGetUint16(DCM_SamplesPerPixel, samplePerPixel);
    OFString laterality, viewCode;
    string viewPosition;
    fileformat.getDataset()->findAndGetOFString(DCM_ImageLaterality, laterality);
    DcmItem *item;
    if (fileformat.getDataset()->findAndGetSequenceItem(DCM_ViewCodeSequence, item).good())
    {
        item->findAndGetOFString(DCM_CodeValue, viewCode);
        if (viewCode == "R-10242")
        {
            viewPosition = "CC";
        }
        else if (viewCode == "R-10226")
        {
            viewPosition = "MLO";
        }
    }
    
    DcmFileFormat fileformatNew(fileformat);
    fileformatNew.getDataset()->putAndInsertString(DCM_PatientName, newPatientName.c_str());
    fileformatNew.getDataset()->putAndInsertString(DCM_PatientID, newPatientID.c_str());
    fileformatNew.getDataset()->putAndInsertString(DCM_SOPClassUID, "1.2.840.10008.5.1.4.1.1.13.1.3");
    fileformatNew.getDataset()->putAndInsertString(DCM_NumberOfFrames, to_string(numFrames).c_str());
    fileformatNew.getDataset()->putAndInsertString(DCM_ImageType, "DERIVED\\PRIMARY\\TOMOSYNTHESIS");
    
    size_t pos = filePath.find_last_of("/");
    string caseFolder = filePath.substr(pos+1);
    string sq = "\'";
    string cmd = "mkdir -p " + sq + filePath + sq;
    system(cmd.c_str());
    string saveToFile = filePath + "/" + string(newPatientID.c_str()) + "_" + string(laterality.c_str()) + viewPosition.c_str() + "_tomo" + ".dcm";
    
    const Uint16 *pPixelData;
    if (!fileformatNew.getDataset()->findAndGetUint16Array(DCM_PixelData, pPixelData).good())
    {
        return -1;
    }
    fileformatNew.getDataset()->remove(DCM_PixelData);
    Uint8 *pEncoded;
    
    OFString transferSyntaxUID;
    DcmElement *elem;
    fileformatNew.getMetaInfo()->findAndGetElement(DCM_TransferSyntaxUID, elem);
    elem->getOFStringArray(transferSyntaxUID);
    if (transferSyntaxUID == UID_JPEGProcess14TransferSyntax || transferSyntaxUID == UID_JPEGLSLosslessTransferSyntax || transferSyntaxUID == UID_JPEG2000LosslessOnlyTransferSyntax || transferSyntaxUID == UID_JPEGProcess14SV1TransferSyntax || transferSyntaxUID == UID_JPEG2000TransferSyntax)
    {
        E_TransferSyntax transferSyntax;
        if (transferSyntaxUID == UID_JPEGProcess14TransferSyntax) transferSyntax = EXS_JPEGProcess14;
        else if (transferSyntaxUID == UID_JPEGProcess14SV1TransferSyntax) transferSyntax = EXS_JPEGProcess14SV1;
        else if (transferSyntaxUID == UID_JPEGLSLosslessTransferSyntax) transferSyntax = EXS_JPEGLSLossless;
        else if (transferSyntaxUID == UID_JPEG2000LosslessOnlyTransferSyntax) transferSyntax = EXS_JPEG2000LosslessOnly;
        else if (transferSyntaxUID == UID_JPEG2000TransferSyntax) transferSyntax = EXS_JPEG2000;
        else return false;
        
        // create initial pixel sequence
        DcmPixelSequence* pixelSequence = new DcmPixelSequence(DCM_PixelSequenceTag);
        if (pixelSequence == NULL)
            return -1;
        
        // insert empty offset table into sequence
        DcmPixelItem *offsetTable = new DcmPixelItem(DCM_PixelItemTag);
        if (offsetTable == NULL)
        {
            delete pixelSequence; pixelSequence = NULL;
            return -1;
        }
        OFCondition cond = pixelSequence->insert(offsetTable);
        if (cond.bad())
        {
            delete offsetTable; offsetTable = NULL;
            delete pixelSequence; pixelSequence = NULL;
            return -1;
        }
        DcmOffsetList dummyList;
        int dataLen;
        pEncoded = new Uint8[width*height*samplePerPixel];
        for (int nFrame = 0; nFrame < numFrames; nFrame++)
        {
            DcmFileFormat fileformatCurrent;
            fileformatCurrent.loadFile(slices[nFrame].c_str());
            // get the compressed pixel data
            getCompressedPixelData(pEncoded, dataLen, fileformatCurrent);
            
            cond = pixelSequence->storeCompressedFrame(dummyList, OFreinterpret_cast(Uint8*,pEncoded), dataLen, 0);
            if (cond.bad())
            {
                delete pixelSequence; pixelSequence = NULL;
                return -1;
            }
        }
        delete []pEncoded;
        // insert pixel data attribute incorporating pixel sequence into dataset
        DcmPixelData *pixelData = new DcmPixelData(DCM_PixelData);
        if (pixelData == NULL)
        {
            delete pixelSequence; pixelSequence = NULL;
            return -1;
        }
        /* tell pixel data element that this is the original presentation of the pixel data
         * pixel data and how it compressed
         */
        pixelData->putOriginalRepresentation(transferSyntax, NULL, pixelSequence);
        cond = fileformatNew.getDataset()->insert(pixelData);
        if (cond.bad())
        {
            cerr << "Error: cannot write DICOM file (" << cond.text() << ")" << endl;
            delete pixelData; pixelData = NULL; // also deletes contained pixel sequence
            return -1;
        }
        status = fileformatNew.saveFile(saveToFile.c_str());
        //delete pixelData;
        //pixelData = nullptr;
        
    }
    else{
        int frameSize = width*height*samplePerPixel;
        Uint16 *pData16 = nullptr;
        Uint8 *pData8 = nullptr;
        const Uint16 *first16 = nullptr;
        const Uint8 *first8 = nullptr;
        Uint16 bitStored;
        fileformat.getDataset()->findAndGetUint16(DCM_BitsStored, bitStored);
        if (bitStored > 8){
            pData16 = new Uint16[numFrames*frameSize];
            fileformat.getDataset()->findAndGetUint16Array(DCM_PixelData, first16);
            memcpy(pData16, first16, sizeof(Uint16)*frameSize);
        }else{
            pData8 = new Uint8[numFrames*frameSize];
            fileformat.getDataset()->findAndGetUint8Array(DCM_PixelData, first8);
            memcpy(pData8, first8, sizeof(Uint8)*frameSize);
        }
        
        for (int k = 1; k < numFrames; k++){
            DcmFileFormat fileformatCurrent;
            fileformatCurrent.loadFile(slices[k].c_str());
            const Uint16 *pFrame16;
            const Uint8 *pFrame8;
            if (bitStored > 8){
                fileformatCurrent.getDataset()->findAndGetUint16Array(DCM_PixelData, pFrame16);
                memcpy(&pData16[k*frameSize], pFrame16, sizeof(Uint16)*frameSize);
            }else{
                fileformatCurrent.getDataset()->findAndGetUint8Array(DCM_PixelData, pFrame8);
                memcpy(&pData8[k*frameSize], pFrame8, sizeof(Uint8)*frameSize);
            }
        }
        
        if (bitStored > 8){
            fileformatNew.getDataset()->putAndInsertUint16Array(DCM_PixelData, pData16, numFrames*frameSize);
        }else{
            fileformatNew.getDataset()->putAndInsertUint8Array(DCM_PixelData, pData8, numFrames*frameSize);
        }
        if (pData16) delete []pData16;
        if (pData8) delete []pData8;
        status = fileformatNew.saveFile(saveToFile.c_str());
    }
    
    return true;
}

void printMetaData(string inputDirectory)
{
    vector<string> files;
    string pattern = "/*.{dcm}";
    int status = collectFiles(inputDirectory, pattern, files);
    if (status < 0) return;
    FILE *flog = fopen("summary.txt", "w");
    string caseId;
    for (int iFile = 0; iFile < files.size(); iFile++)
    {
        string extension;
        size_t dotpos = files[iFile].find_last_of(".");
        extension = files[iFile].substr(dotpos+1);
        if (extension != "dcm") continue;
        DcmFileFormat fileformat;
        OFCondition status = fileformat.loadFile(files[iFile].c_str());
        if (!status.good()){
            cout << "Fail to read the image: " << files[iFile] << endl;
            return;
        }
        
        OFString manufacturer, manufacturerModelName;
        if (fileformat.getDataset()->findAndGetOFString(DCM_Manufacturer, manufacturer).good())
        {
            
        }else{
            cout << "The manufacturer field is missing!" << endl;
            return;
        }
        
        if (fileformat.getDataset()->findAndGetOFString(DCM_ManufacturerModelName, manufacturerModelName).good())
        {
            
        }else{
            cout << "The manufacturer model name field is missing!" << endl;
            return;
        }
        
        string imageName;
        string caseFolder = files[iFile].substr(inputDirectory.length()+1);
        size_t pos = caseFolder.find_first_of("/");
        caseFolder = caseFolder.substr(pos+1);
        pos = caseFolder.find_first_of("/");
        caseFolder = caseFolder.substr(0, pos);
        if (caseFolder != caseId)
        {
            fprintf(flog, "\n\nCase %s", caseFolder.c_str());
            caseId = caseFolder;
        }
        pos = files[iFile].find_last_of("/");
        imageName = files[iFile].substr(pos+1);
        fprintf(flog, "\nimage: %s Manufacturer: %s Manufacturer model: %s", imageName.c_str(), manufacturer.c_str(), manufacturerModelName.c_str());
    }
    fclose(flog);
}

int main(int argc, const char * argv[])
{
    if (argc < 2){
        cout << "Please specify the path to the text file!" << endl;
        exit(1);
    }
    
    string inputDirectory = argv[1];
    FILE *flog;
    ifstream fin(inputDirectory);
    if (!fin.good()){
        cout << "The text file: " << inputDirectory << " doesn't exist!" << endl;
        return -1;
    }
    string line;
    string inputPath, outputPath, revealOutputPath, caseId;
    int copyTomo = 0, runFlag = 0;
    bool startList = false, fixPriors = false;
    bool copyDuplicatesFromSameStudy = false, copyFollowUpData = false, copyCTtoTomo = false, copyOther = false;
    cout << "Start reading flags from text file" << endl;
    while (getline(fin, line))
    {
        istringstream in(line);
        string name;
        in >> name;
        if (name == "OrigPath"){
            in >> inputPath;
            startList = false;
            cout << "Reading OrigPath is done" << endl;
        }
        else if (name == "ToolPath"){
            in >> outputPath;
            string textFile = outputPath+"summary.txt";
            flog = fopen(textFile.c_str(), "w");
            if (flog == nullptr){
                cout << "Cannot create summary.txt at output path: " << textFile << endl;
                exit(-1);
            }
            cout << "Reading ToolPath is done" << endl;
        }
        else if (name == "RevealPath"){
            in >> revealOutputPath;
            cout << "Reading RevealPath is done" << endl;
        }
        else if (name == "CopyTomo"){
            in >> copyTomo;
            cout << "Reading CopyTomo is done" << endl;
        }
        else if (name == "CopyCTtoTomo"){
            in >> copyCTtoTomo;
            cout << "Reading CopyCTtoTomo is done" << endl;
        }
        else if (name == "CopyOther"){
            in >> copyOther;
            cout << "Reading CopyOther is done" << endl;
        }
        else if (name == "RunFlag"){
            string run;
            in >> run;
            runFlag = stoi(run);
            cout << "Reading RunFlag is done" << endl;
        }
        else if (name == "DontCopyDuplicatesFromSameStudy"){
            in >> copyDuplicatesFromSameStudy;
            copyDuplicatesFromSameStudy = !copyDuplicatesFromSameStudy;
            cout << "Reading DontCopyDuplicatesFromSameStudy is done" << endl;
        }
        else if (name == "DontCopyFollowUpData"){
            in >> copyFollowUpData;
            copyFollowUpData = !copyFollowUpData;
            cout << "Reading DontCopyFollowUpData is done" << endl;
        }
        else if (name == "FixPriors"){
            in >> fixPriors;
            cout << "Reading FixPriors is done" << endl;
        }
        else if (name == "#CaseID"){
            startList = true;
            continue;
        }
        string inputFile, prevFolder;
        if (startList && name != ""){
            inputFile = inputPath + name;
            inputFile.replace(inputFile.length()-1, 1, "/");
            caseId = name.substr(0, name.length()-1);
            cout << "Start processing case " << caseId << endl;
            in >> name;
            string newPatientName = name.substr(0, name.length()-1);
            
            in >> name;
            string newPatientID = name.substr(0, name.length()-1);
            
            in >> name;
            string Accession = name.substr(0, name.length()-1);
            
            in >> name;
            string newStudyDate = name.substr(0, name.length());
        
            // Find out the paths of images in the input directory
            vector<string> files;
            string pattern = "/*";
            int status = collectFiles(inputFile, pattern, files);
            if (status < 0) continue;
            if (files.size() == 0)
            {
                cout << "No images are found in directory: " << inputFile << endl;
            }
            tomoView.clear();
            otherView.clear();
            standard2dView[0].clear();standard2dView[1].clear();standard2dView[2].clear();standard2dView[3].clear();
            ctSlices[0].clear();ctSlices[1].clear();ctSlices[2].clear();ctSlices[3].clear();
            followUp[0] = followUp[1] = followUp[2] = followUp[3] = 1;
            
            string filePath = outputPath + caseId;
            // Loop through each image
            vector<string> processed, ignored;
            for (int iFile = 0; iFile < files.size(); iFile++){
                size_t pos = files[iFile].find_last_of("/");
                string imageName = files[iFile].substr(pos+1);
                size_t dotpos = imageName.find_last_of(".");
                if (dotpos != string::npos){
                    string extension = imageName.substr(dotpos+1);
                    if (extension != "dcm") continue;
                }
                
                viewInfo info;
                collectViews(files[iFile]);
            }
            
            string suffix;
            modifiedMetadata metaData;
            metaData.newPatientName = newPatientName;
            metaData.newPatientID = newPatientID;
            metaData.accessionNumber = Accession;
            metaData.studyDate = newStudyDate;
            // Copy standard 2d views
            for (int j = 0; j < standard2dView.size(); j++){
                if (standard2dView[j].size() == 0)continue;
                // Always copy the most recent 2d view
                suffix = "_2d_" + to_string(0);
                modifyMetaData(standard2dView[j][followUp[j]-1].fileName, metaData, fixPriors, filePath, suffix);
                processed.push_back(standard2dView[j][followUp[j]-1].fileName);
                // Copy the duplicates views if it exist
                if (copyDuplicatesFromSameStudy){
                    for (int k = 0; k <followUp[j]-1; k++){
                        suffix = "_2d_" + to_string(k+1);
                        modifyMetaData(standard2dView[j][k].fileName, metaData, fixPriors, filePath, suffix);
                        processed.push_back(standard2dView[j][k].fileName);
                    }
                }
                else{
                    for (int k = 0; k <followUp[j]-1; k++){
                        ignored.push_back(standard2dView[j][k].fileName);
                    }
                }
            }
            
            if (runFlag == 1){
                // run reveal on standard 2d views
                string outDirectory = revealOutputPath + caseId;
                mkdir(outDirectory.c_str(), S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
                
                string command = "./ICEReveal " + filePath + " " + outDirectory + " 1";
                system(command.c_str());
            }
            
            // Copy follow up data if it exist
            for (int j = 0; j < standard2dView.size(); j++){
                if (copyFollowUpData && followUp[j] < standard2dView[j].size()){
                    for (int k = followUp[j], index = 0; k < standard2dView[j].size(); k++, index++){
                        suffix = "_followup_" + to_string(index);
                        modifyMetaData(standard2dView[j][k].fileName, metaData, fixPriors, filePath, suffix);
                        processed.push_back(standard2dView[j][k].fileName);
                    }
                }
                else{
                    for (int k = followUp[j]; k < standard2dView[j].size(); k++){
                        ignored.push_back(standard2dView[j][k].fileName);
                    }
                }
            }
            
            if (copyTomo && copyCTtoTomo){
                // Generate tomo image from CT slices
                for (int k = 0; k < ctSlices.size(); k++){
                    if (ctSlices[k].size() > 0){
                        createTomo(ctSlices[k], newPatientName, newPatientID, filePath);
                    }
                }
            }
            
            for (int k = 0; k < tomoView.size(); k++){
                if (copyTomo){
                    suffix = "_tomo_" + to_string(k);
                    modifyMetaData(tomoView[k], metaData, fixPriors, filePath, suffix);
                    processed.push_back(tomoView[k]);
                }else{
                    ignored.push_back(tomoView[k]);
                }
            }
            for (int k = 0; k < otherView.size(); k++)
            {
                if (copyOther){
                    suffix = "_other_" + to_string(k);
                    modifyMetaData(otherView[k], metaData, fixPriors, filePath, suffix);
                    processed.push_back(otherView[k]);
                }else{
                    ignored.push_back(otherView[k]);
                }
            }
            
            // output the summary info
            fprintf(flog, "\n\nCase:%s", caseId.c_str());
            fprintf(flog, "\n LCC:%lu LMLO:%lu RCC:%lu RMLO:%lu", standard2dView[0].size(), standard2dView[1].size(), standard2dView[2].size(), standard2dView[3].size());
            fprintf(flog, "\n TOMO views:%lu Other views:%lu", tomoView.size(), otherView.size());
            fprintf(flog, "\nProcessed files:");
            for (int k = 0; k < processed.size(); k++){
                fprintf(flog, "\n%s", processed[k].c_str());
            }
            fprintf(flog, "\nIgnored files:");
            for (int k = 0; k < ignored.size(); k++){
                fprintf(flog, "\n%s", ignored[k].c_str());
            }
            fprintf(flog, "\nManufacturer: %s", manufacturer.c_str());
            fprintf(flog, "\nManufacturer model: %s", manufacturerModel.c_str());
        }
    }
    fclose(flog);
    return 0;
}
