//
//  fileUtil.h
//  Density
//
//  Created by Anna Yang on 4/24/18.
//  Copyright © 2018 imagosystems. All rights reserved.
//

#ifndef fileUtil_h
#define fileUtil_h

#ifdef _WIN64
    #include <windows.h>
#else
    #include "glob.h"
    #include <dirent.h>
    #include <sys/stat.h>
#endif

#include <stdio.h>
#include <vector>
using namespace std;

int collectFiles(string name, string pattern, vector<string>& files){
    DIR *dir;
    struct dirent *entry;
    
    if (!(dir = opendir(name.c_str()))){
        cout << "Cannot open folder!" << endl;
        return -1;
    }
    
    //glob_t glob_result;
    //string searchPattern = name + pattern;
    //glob(searchPattern.c_str(), GLOB_BRACE, NULL, &glob_result);
    //for(size_t i = 0; i < glob_result.gl_pathc; ++i) {
        //files.push_back(string(glob_result.gl_pathv[i]));
    //}
    
    while ((entry = readdir(dir)) != NULL) {
        if (entry->d_type == DT_DIR) {
            if (strcmp(entry->d_name, ".") == 0 || strcmp(entry->d_name, "..") == 0)
                continue;
            
            if (name.substr(name.length()-1) != "/") name = name + "/";
            string path = name + string(entry->d_name);
            collectFiles(path, pattern, files);
        }
        else{
            if (name.substr(name.length()-1) != "/") name = name + "/";
            string file = name + string(entry->d_name);
            files.push_back(file);
        }
    }
    closedir(dir);
    return 0;
}

#endif /* fileUtil_h */
